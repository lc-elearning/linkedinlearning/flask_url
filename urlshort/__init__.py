from flask import Flask


def create_app(test_config=None):
    app = Flask(__name__)  # __name__ => name of current running module
    app.secret_key = "grg5zgh5$£^ s6"  # secret key should be random and complex for production

    from . import urlshort
    app.register_blueprint(urlshort.bp)

    return app
